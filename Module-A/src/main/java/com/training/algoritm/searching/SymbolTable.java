package com.training.algoritm.searching;

public interface SymbolTable<K,V> {

    public abstract void put(K key, V value);

    public abstract V get(K key);

    public abstract void delete(K key);

    public abstract boolean contains(K key);

    public abstract boolean isEmpty(K key);

    public abstract int size();

    public abstract Iterable<K> keys();
}
