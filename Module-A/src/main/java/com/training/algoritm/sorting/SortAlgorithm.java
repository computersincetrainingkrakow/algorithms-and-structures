package com.training.algoritm.sorting;

import java.util.Comparator;

public abstract class SortAlgorithm {
    abstract public void sort(Comparable[] a);

    public boolean less(Comparable v, Comparator w){
        return v.compareTo(w) < 0;
    }

    public void exch(Comparable[] a, int i, int j){
        Comparable t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
